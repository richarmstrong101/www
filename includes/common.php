<?php
//in this document i define functions to be called elsewhere

//i define pr which is commonly used within PHP to format the print_r function in a more accessible format.
function pr($var = array()) {
    echo '<pre>';
    print_r($var);
    echo '</pre>';
    $line = debug_backtrace();
    echo $line[0]['file'] . ' - Line ' . $line[0]['line'];
}
//has very similar functionality to pr but does not feature the information regarding the file root
function pre($var = array()){
    echo '<pre>';
    print_r($var);
    echo '</pre>';
}

function pr1($var = array()){
	echo '<pre>';
    print_r($var[0]);
    echo '</pre>';
}


//isuserloggedin is a very self explanatory function which checks whether the user is logged in and can be used to grant additional functionality to logged in users or administrative privileges if they are required.
function isuserloggedin(){
    if (isset($_SESSION['user'])) {
        return true;
    }
    return false;
}