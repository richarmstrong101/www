<?php


class DB {
	
// creation of database functions
	public $pdo = '';


	public function __construct($database_object)
	{
		$this->pdo = $database_object;
	}

// prepares queries and defines all results (pr())

	public function getData($query = '', $parameters = array())
	{
		$data = $this->pdo->prepare($query);

		if ( ! empty($parameters)) {

			foreach ($parameters as $key => $var) {

				$data->bindValue($key, $var);

			}

		}
		$res = $data->execute();

		return $data->fetchall(PDO::FETCH_ASSOC);

		die;

		
	}

	// this returns a bool to confirm if it worked
	public function insertData ($query = '', $paramaters = array()){

		$result = $this->pdo->prepare($query);

		$worked = $result->execute($paramaters);

		return $worked;
	}


	public function updateData ($query = '', $paramaters = array()){
		$result = $this->pdo->prepare($query);

		$worked = $result->execute($paramaters);

		return $worked;
	}

	// public function deleteData ($query = '', $parameters = ()){
	// 	$result = $this->pdo->prepare($query);

	// 	$worked = $result->execute($paramaters);

	// 	return $worked;
	// }
}
