<?php 
$link = new PDO('mysql:host=localhost;dbname=helpdesk', 'root', '');

$list = $link->prepare('
	SELECT 
		id, todo, user
	FROM 
		todo
	WHERE 
		user = "'.$_SESSION['user'].'"
		'
);

$list-> execute();

$result = $list->fetchall();

?>
<h2 style="text-align: center;">
	This is your to do list
</h2>
<ul>
	<?php if (!empty($result)) : ?>
		<p>
			It looks like you have to:
		</p>
	<form action="" method="POST">
		<ol>
			<?php foreach ($result as $var) : ?>
				<li>
					<?=$var['todo'];?> - Have you finished this task?
						<input type="checkbox" name="done" value="<?=$var['id']?>" onChange="this.form.submit()">
				</li>
			<?php endforeach;?>
		</ol>
	<input type="hidden" name="form_type" value="todofinished" />
</form>



		<h3> That means that you only have 
		<?= count($result);?> task<?php if (count($result) > 1){echo "s";}else{echo "";}?> left!</h3>
	<?php else : ?>
	<div class="center">	<h2>Nothing on your to-do list! Nice!</h2> </div>
	<?php endif ;?>

	<br />
	<form action="" method="post">
		<label><h3>Add To Your To-Do List! </h3><input type="text" name="todo" /></label>
		<input type="hidden" name="user" value="<?=ucfirst($_SESSION['user']);?>">
		<input type="hidden" name="form_type" value="todo" />
		<div class="center"><input type="submit" value="Add To-Do" /></div>
	</form>
</ul>
<br />
<div class="center"> <a href="../">Click here to go home.</a></div>