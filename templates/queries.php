<h1>
    Search for your query ID below
</h1>
<br>

<!-- here the GET method was used because the query ID is not something which needs to be kept safe. -->
<form action="<?=$_SERVER['REQUEST_URI'];?>" method="GET">
    <p>
        <label>Click here to see all queries </label>
        <input style="width:20px;height:20px;" id="checkBox" type="checkbox" name="test" onChange="this.form.submit()" />
    <input type="hidden" name="form_type" value="all" /></p>
</form>

<form action="<?=$_SERVER['REQUEST_URI'];?>" method="GET"><div class="center">
    <label>Search</label><input style="width: 100px; margin-left: 10px;" name="id" type="text" placeholder="Enter your query ID here" value="" />
    <input style="margin-left: 10px;" type="submit" />
    <input type="hidden" value="resolved" name="form_type" />
</form></div><br />

<?php if(isset($get_data) && (!empty($get_data))) : ?>
    <table>
<!-- table creation for a nice layout -->
    <tr>
        <td>Query ID</td>
        <td>Employee Name</td>
        <td>Employee ID</td>
        <td>Problem Description</td>
        <td>Item With Issue</td>
        <td>Serial Number</td>
        <td>Helpdesk ID</td>
        <td>Severity</td>
        <td>Resolved</td>
        <td>The Fix</td>
        <td>The Call Reciever</td>
    </tr>
<!-- basic php foreach to iterate over an array and allow each value to be printed under its suitable heading. -->
    <?php if ( ! empty($get_data)) : foreach ($get_data as $var) : ?>

        <tr>
            <td><?=$var['id'];?></td>
            <td><?=$var['empname'];?></td>
            <td><?=$var['empid'];?></td>
            <td><?=$var['problem'];?></td>
            <td><?=$var['item'];?></td>
            <td><?=$var['serialno'];?></td>
            <td><?=$var['helpid'];?></td>
            <td><?=ucfirst($var['severe']);?></td>
            <!-- this is a nice line of code which makes the table more readable -->
            <td><?=($var['resolved']==0) ? "Not resolved" : "Resolved";?></td>
            <td><?=$var['the_fix'];?></td>
            <td><?=$var['callreceiver'];?></td>
        </tr>
    <?php endforeach;?> 
</table>
    
<?php endif; ?>
<?php endif; ?>