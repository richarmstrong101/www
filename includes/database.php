<?php
//creates a pdo to be used to connect to a database. This is one of the three available APIs.
$pdo = new PDO('mysql:host=' . DB_HOST . ';dbname=default', DB_USER, DB_PASS);