# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.38)
# Database: helpdesk
# Generation Time: 2018-02-18 16:15:35 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table queries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `queries`;

CREATE TABLE `queries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `empname` text NOT NULL,
  `empid` text NOT NULL,
  `problem` text NOT NULL,
  `item` varchar(255) NOT NULL,
  `serialno` int(255) NOT NULL,
  `helpid` int(255) NOT NULL,
  `helpname` text,
  `severe` varchar(255) NOT NULL,
  `resolved` tinyint(1) DEFAULT NULL,
  `the_fix` text,
  `os` text NOT NULL,
  `software` text NOT NULL,
  `callreceiver` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

LOCK TABLES `queries` WRITE;
/*!40000 ALTER TABLE `queries` DISABLE KEYS */;

INSERT INTO `queries` (`id`, `empname`, `empid`, `problem`, `item`, `serialno`, `helpid`, `helpname`, `severe`, `resolved`, `the_fix`, `os`, `software`, `callreceiver`)
VALUES
	(1,'Terry','132','Q key not working on keyboard','Keyboard',131,1,'Rich','high',1,'Replaced keyboard','','',''),
	(2,'Dave','132','Q key not working on keyboard','Keyboard',131,1,'Rich','high',1,'turn it off and on again','','',''),
	(3,'Dave','11','Mouse has stopped working','Mouse',131,1,'Rich','mid',1,'Replaced mouse','psn','',''),
	(4,'Adem','1828','There has been a problem with the monitor switching off','Monitor',28329,1,'Rich','low',1,'Unfucked it','psn','',''),
	(5,'Dave','11','Q key not working on keyboard','Mouse',11,1,'Rich','high',0,NULL,'windows','word',''),
	(6,'Rich','1','test','test',1,1,NULL,'low',NULL,NULL,'test','test',''),
	(7,'Rich','1','keyboard not working','keyboard',123,1,'Rich','high',1,'Replacing the keyboard','Windows','',''),
	(8,'Rich','1','keyboard issue','keyboard',0,1,NULL,'low',NULL,NULL,'Windows','','');

/*!40000 ALTER TABLE `queries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table todo
# ------------------------------------------------------------

DROP TABLE IF EXISTS `todo`;

CREATE TABLE `todo` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `todo` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `helpid` int(11) NOT NULL,
  `firstname` text NOT NULL,
  `surname` text NOT NULL,
  `skills` text NOT NULL,
  `jobson` int(11) NOT NULL,
  `description` text NOT NULL,
  `totaljobs` int(11) NOT NULL,
  `startdate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `user`, `pass`, `helpid`, `firstname`, `surname`, `skills`, `jobson`, `description`, `totaljobs`, `startdate`)
VALUES
	(1,'Adem','Dragon1887',1,'Adem','Kanca','networking hardware, peripherals & windows 10',4,'Adem is passionate about rocket league, he also sometimes does essays and writes CSS',40,'2017-08-13'),
	(2,'Liam','Dragon1887',2,'Liam','Mason','networking hardware, peripherals & windows 10',2,'Adem is passionate about rocket league, he also sometimes does essays and writes CSS',84,'2017-06-18'),
	(3,'Jack','BLM1994',3,'Jack','Nickleson','system monitoring, web programming & back end system analysis',1,'gdfdfgfd',103,'2017-06-15'),
	(4,'Amy','Password123',4,'Amy','Gressier','hardware, Mac operating systems & networking software',6,'Rich really enjoys coding PHP and HTML. However JavaScript and CSS and the banes of his existence.',36,'2017-08-24'),
	(5,'Tasman','Password123',5,'Tasman','Hook','hardware, Mac operating systems & networking software',3,'Rich really enjoys coding PHP and HTML. However JavaScript and CSS and the banes of his existence.',18,'2018-01-19'),
	(6,'Rich','Password123',6,'Richard','Armstrong','PHP, SQL, networking hardware, software debugging',3,'Rich really enjoys coding PHP and HTML. However JavaScript and CSS and the banes of his existence.',50,'2017-05-26');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
