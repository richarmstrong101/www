<?php 
$link = new PDO('mysql:host=localhost;dbname=helpdesk', 'rich', '');

$list = $link->prepare('
    SELECT 
        *
    FROM 
        users
    ORDER BY 
    jobson
    ASC
    ');

$list-> execute();

$result = $list->fetchall();

?>

<h1>Please enter some details of your issue below <br /> <h3>All fields are required.</h3></h1> <br />

<table border="1" width="100%">
    <form action="" method="POST">
        <tr>
            <td><p>What is your name?</p></td>
            <!-- //in this document i have several checks which set the value as equal to either an element defines within $_SESSION to save time or empty if the user is not logged in -->
            <td><input type="text" name="empname" placeholder="What is your name?" value='<?=(isuserloggedin() ? $_SESSION['user'] : "" );?>' /></td>
        </tr>        
        <tr>
            <td><p>What is the callers ID number</p></td>
            <td><input type="text" name="empid" placeholder="Enter the callers ID number" /></td>
        </tr>
        <tr>
            <td><p>What is your ID number?</p></td>
            <td><input type="text" name="callreceiver" placeholder="Enter your ID number here" value='<?=(isuserloggedin() ? $_SESSION['user_id'] : "" );?>' /> </td>
        </tr>

        <tr>
            <td><p>Issue Description (e.g. Some keys on keyboard are not responding)</p></td>
            <td><input type="text" name="problem" placeholder="Describe The Issue" /></td>
        </tr>
        <tr>
            <td><p>Item</p></td>
            <td><input type="text" name="item" placeholder="What Item Does The Issue Relate To" /></td>
        </tr>
        <tr>
            <td><p>Serial Number</p></td>
            <td><input type="text" name="serialno" placeholder="Product Serial Number" /></td>
        </tr>
        <tr>
            <td><P>Severity of Issue</P></td>
            <td><div class="dropdown">
                    <select name="severe">
                        <option value="low">Workable</option>
                        <option value="mid">Disruptive</option>
                        <option value="high">Totally unusable</option>
                    </select>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <p>What is the operating system? (If applicable)</p>
            </td>
            <td><input type="text" name="os" placeholder="Operating System"></td>
        </tr>
        <tr>
            <td>
                <p>What is the software? (If applicable)</p>
            </td>
            <td><input type="text" name="software" placeholder="Software"></td>
        </tr>
        <tr>
            <td>
                <p>Who is this ticket going to?</p>
            </td>
            <td>
                <input type="text" name="helpid" value="<?php print_r($result[0][1]);?>" />
            </td>
        </tr>
        <tr class="submit">
            <td colspan="2">
                <input type="hidden" name="form_type" value="create" />
                <input type="submit" value="Submit" />
            </td>
        </tr>
    </form>
</table>

