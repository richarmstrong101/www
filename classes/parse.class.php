<?php


class Parse {

//creates functions to call url and check the login status.
	public $url_data = array();

	public $login_required_page = array(
		'search',
		'query'
	);

	public function __construct($DB)
	{
		$this->db = $DB;
	}

//this is to check the URL 
	public function parseUrl()
	{
		$url = parse_url($_SERVER['REQUEST_URI'])['path'];

		$url_explode = array_filter(explode('/', $url));

		$this->url_data['pages'] = $url_explode;



		$this->url_data['page'] = $this->parsePage();

		return $this->url_data;
	}


//this is to ensure that the page is accessible. 
	public function parsePage()
	{
		$page = array_reverse($this->url_data['pages']);

		// remove get param from url
		// foreach ($page as $key => $var) {
		// 	if ( strpos($var, '?') ) {
		// 		$remove_get = array_filter(explode('?', $var));
		// 		$page[$key] = $remove_get[0];
		// 	}
		// }
		
		$output['template'] = 'templates/404.php';
		$output['title'] = '404 - Page not found';

		if ( empty($page) ) {

			$output['template'] = 'templates/home.php';
			$output['title'] = 'Helpdesk';
		}
		elseif ( ! isuserloggedin() && in_array($page[0], $this->login_required_page)) {
			header('Location: /login');
			die;
		} else if ($page[0] == 'logout' && count($page) == 1) {

			session_destroy();
			header('Location: /');
			die;

		} else if ( file_exists('templates/' . $page[0] . '.php') && count($page) == 1 ) {

			$output['template'] = 'templates/' . $page[0] . '.php';
			$output['title'] = ucfirst($page[0]);

		}

		return $output;
	}

//if post's are used they will usually be called within this function and if they're to be used or have a function commited, it will be done in this function.
	public function parsePOST()
	{
//this checks that the post is set and not empty.
		if (isset($_POST) && ! empty($_POST)) {
//form_type is set in the form on each individual document. 
			if (isset($_POST['form_type'])) {

				switch ($_POST['form_type']) {
//each case is defined in each page so the form knows which functions to run. 
					// login is the login page which allows the user to login from the users database
					case 'login':

						$user = $this->db->getData(
								'SELECT 
									id, user, pass 
								FROM 
									users 
								WHERE 
									user = :username 
								and 
									pass = :password',
								array(
									':username' => $_POST['username'],
									':password' => $_POST['password']
								)
							);
						//this creates sessions which are used everywhere over the site to make things easier for the user.
		
						if ( ! empty($user) ) {
							$_SESSION['user_id'] = $user[0]['id'];
							$_SESSION['user'] = $user[0]['user'];
							header('Location: /home');
						} else {
							
							unset($_SESSION['user_id']);
							unset($_SESSION['user']);
							header('Location: /incorrect');
						}
						die;

					break;

					case 'create':
						//this case it to create a query. Inserts into the database.
						$new = $this->db->insertData(
						'INSERT INTO
							queries
						(empname, empid, problem, item, serialno, helpid, severe, os, software, callreceiver)
						VALUES 
						(:empname, :empid, :problem, :item, :serialno, :helpid, :severe, :os, :software, :callreceiver)
						',
						//this is the first place you see the array function being used, this allows gets and posts to be used to define sql queries
							array(
					    ':empname' => $_POST['empname'],
						':empid' => $_POST['empid'],
						':problem' => $_POST['problem'],
						':item' => $_POST['item'],
						':serialno' => $_POST['serialno'],
						':helpid' => $_POST['helpid'],
						':severe' => $_POST['severe'],
						':os' => $_POST['os'],
						':software' => $_POST['software'],
						':callreceiver' => $_POST['callreceiver']
						));
					return $new;
					break;


					//as it sounds this is the contact function which is used to send emails to the default email database. Without using functions which are created online. Which isn't allowed, this means doing it simply directs all messages to the spam folder.
					case 'contact':
						
						if (isuserloggedin() == true) { 

							$mailsent = mail(
							"helpdeskdemo123@gmail.com", #being sent to - the password is DemoPassword
							"Subject" . " - " .$_POST['subject'], #subject of the email
							"Email sent from " . $_SESSION['user'] . ". The message is - " . $_POST['message'], #the message
							"From: demo.email@example.com"
							);
							if ($mailsent == 1) {
								header('Location: /emailsent');
							}else{header('Location: /emailnotsent');}
						} else {echo "Please log in to send an email!";
						}

					break;
					
					case 'resolvedin':
					//I use this case to update the table when a query has been resolved. this creates the fixes and tells the database who has fixed it with their ID and name.
						$_POST['resolved'] = "1"; 
						$resin = $this->db->updateData('
							UPDATE 
								queries		
							SET 
							helpid = :helpid, helpname = :helpname, resolved = :resolved, the_fix = :the_fix
							WHERE
							id = :id
						',
						array(
						':id' => $_POST['id'],
						':helpid' => $_POST['helpid'],
						':helpname' => $_POST['helpname'],
						':the_fix' => $_POST['the_fix'],
						':resolved' => $_POST['resolved']
						// ':' => $_POST[''],
						));

					break;
					//created something extremely simple for a todo list as it wasn't defined as necessary in the spec
					case 'todo':
						$new = $this->db->insertData(
						'INSERT INTO
							todo
						(id, todo, user)
						VALUES 
						(:id, :todo, :user)
						',
						
							array(
					    'id' => $_POST['id'],
					    'todo' => $_POST['todo'],
					    'user' => $_POST['user']
						));

					return $new;
						break;

					case 'todofinished':
						$delete = $this->db->insertData(
							'DELETE FROM
							todo 
							WHERE 
							id = :id', 
								array(
							'id'=>$_POST['done'])
						);
						return $delete;
						break;

					//if no case is found return the default class which does nothing.
					default:
						// Do nothing
					break;

				}

			}

		}

		return array();
	}




//same as previous function but for GET instead of POST form types
public function parseGET()
	{
		if (isset($_GET) && ! empty($_GET)) {

			if (isset($_GET['form_type'])) {

				switch ($_GET['form_type']) {

					//this is the case i used on the resolved page to display the query they have resolved. Furthermore this allows the form to be autofilled with the details.
						case 'resolved':

							if (isset($_GET['id']) && !empty($_GET['id'])){

							$resolved = $this->db->getData(
								'
								SELECT 
								*
								FROM 
								queries
								WHERE id = :id
								',
								 array(
								 	':id' => $_GET['id'] 
								 )
							);
							return $resolved;
							} else {
								return;
							}
							break;


							case 'search':

							if($_GET['test'] == on){
							$resolved = $this->db->getData(
								'
								SELECT 
								*
								FROM 
								queries');
							return $resolved;
							echo $_GET['test'];
							echo "test working kind of";
							}else{echo "not working";}
							var_dump($_GET);
							echo "yes";
						break;

						case 'all':

						$resolved = $this->db->getData('
						SELECT 
							*
						FROM 
							queries
						'
						);
						return $resolved;
						break;



					default:
						// Do nothing
					break;

				}

			}

		}

		return array();
	}


}