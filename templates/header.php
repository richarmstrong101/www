<!doctype html>
<!-- //this header is going to be called on every single page as set in the index. As a result the layout is going to feel familar on every page -->
<html>
<head>
	<!-- // this is a nice way of setting the page title -->
	<title>
		<?php 
		#here i designed the most remarkably efficient way of setting a variable title possibly ever

			$urltitle = str_replace("/", "", strtolower($_SERVER['REQUEST_URI']));

			if ($urltitle === "emailnotsent") {
				echo "Email Not Sent";
			}elseif ($urltitle === "emailsent"){
				echo "Success";
			}elseif ($urltitle === "querylist"){
				echo "Queries";
			}elseif ($urltitle === "resolvedin"){
				echo "resolved";
			}elseif ($urltitle === ""){
				echo "Home Page";
			}elseif (strtolower(substr($_SERVER['REQUEST_URI'], 1, 7)) === "queries"){
				echo "Queries";
			}elseif (strtolower(substr($_SERVER['REQUEST_URI'], 1, 11)) === "specialists"){echo "Specialists";
			}else{echo ucfirst($urltitle);}
		?>
			<?=SEPERATOR;?>
			<?=TITLE_APPEND?>
	</title>
		<?php
			// str_replace("/", "", strtolower($_SERVER['REQUEST_URI'])) == "test" || str_replace("/", "", strtolower($_SERVER['REQUEST_URI'])) == "contact" || str_replace("/", "", strtolower($_SERVER['REQUEST_URI'])) == "specialists" || str_replace("/", "", strtolower($_SERVER['REQUEST_URI'])) == "resolved"
		 // || "contact" || "specialists" || "resolved"

				?>
	<link rel="stylesheet" type="text/css" href="/assets/css/format.css" />
</head>
<body>
	<header>
		<h1><?=$url_array['page']['title'];?></h1>
		<!-- //this either greets the user with their name or encourages them to log in -->
		<?php if (isuserloggedin()) : ?>
			<div class="status">
				<p>Hi, <?=ucfirst($_SESSION['user']);?></p>
			</div>
			<!-- in this list i define what the user is shown if they are logged in. This is to stop people who aren't logged in accessing potential sensitive information. -->
			<div class="menu-list">
				<nav>
					<ul class="menu menuloggedin">
						<li><a href="/">Home</a></li>
						<li><a href="/queries">Queries</a></li>
						<li><a href="/contact">Contact</a></li>
						<li><a href="/create">Create a <br /> Ticket</a></li>
						<li><a href="/specialists">See specialists</a></li>
						<li><a href="/resolved">Mark Resolved</a></li>
						<li><a href="/logout">Logout</a></li>	
					</ul>
				</nav>
			</div>
		<?php else : ?>
			<div class="status">
				<p>You're not logged in - <a href="/login"> Please login </a></p>
			</div>
			<div class="menu-list">
				<nav>
					<ul class="menu menuloggedout">
						<li><a href="/">Home</a></li>
						<li><a href="/contact">Contact</a></li>
						<li><a href="/runpy">Run Python Page</a></li>
					</ul>
				</nav>
				
			</div>
		<?php endif; ?>
	<div class="clearboth"></div>
	</header>