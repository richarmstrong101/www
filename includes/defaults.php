<?php
//defines the default settings for database connection. 
define('DB_HOST', 'localhost');

switch ($_SERVER['HTTP_HOST']){
	case "helpdesk.com" : 
	case "213.251.43.134" :
	case "www.helpdesk.com" :
		define('DB_USER', 'root');
		break;

	default :
		define('DB_USER', 'root');
		break;
}

define('DB_PASS', '');

define('TITLE_APPEND', 'Helpdesk');
define('SEPERATOR', ' | ');