<h3>Mark a query as completed from here!</h3>


<form action="" method="GET">
    <label style="align-content: center;">Resolved ID<input name="id" type="text" placeholder="Enter your query ID here"/></label>
    <input type="hidden" name="form_type" value="resolved" />
    <div style="text-align: center;"><input type="submit" value="Check Query"/></div>
</form>




<br />
<?php if (!empty($get_data)): ?>
<table>
	<tr>
		<th>Query ID</th>
		<th>Employee Name</th>
		<th>Helpdesk ID</th>
		<th>Problem</th>
		<th>Item</th>
		<th>Serial Number</th>
		<th>Helpdesk Employee Number</th>
		<th>Helpdesk Employee Name</th>
		<th>Severity</th>
		<th>Is the issue resolved?</th>
		<th>What is the fix?</th>
	</tr>

 <?php if ( ! empty($get_data)) : foreach ($get_data as $var) : ?>

	<tr>
		<td><?= $var['id'];?></td>
		<td><?= $var['empname'];?></td>
		<td><?= $var['empid'];?></td>
		<td><?= $var['problem'];?></td>
		<td><?= $var['item'];?></td>
		<td><?= $var['serial'];?></td>
		<td><?= $var['helpid'];?></td>
		<td><?= ucfirst($var['helpname']); ?></td>
		<td><?= ucfirst($var['severe']);?></td>
		<?=//this is a basic if statement in PHP.?>
		<td><?= ($var['resolved']==0) ? "Not resolved" : "Resolved";?></td>
		<td><?= $var['the_fix'];?></td>
	</tr>

    <?php endforeach; else : ?>

    	<tr>
    		<td colspan="11">No data found</td>
    	</tr>

	<?php endif; ?>
<?php endif;?>
</table>
<br />
<table>
	<form action="" method="POST">
		<h2 style="text-align: center;">Enter the fix here!</h2>
		<?=// this form is auto-filled with the logged in users information?>
		<tr>
			<th>What is the Query ID?</th>
			<td><input type="text" name="id" value="<?=isset($var['id']) ? $var['id'] : "" ;?>"></td>
		</tr>
		<tr>
			<th>What is was fix?</th>
			<td><input type="text" name="the_fix" placeholder="Enter your Fix here" value="" /></td>
		</tr>
		<tr>
			<th>What is your name?</th>
			<td><input type="text" name="helpname" value="<?=isuserloggedin() ? $_SESSION['user'] : "" ;?>"></td>
		</tr>
		<tr>
			<th>What is your ID number?</th>
			<td><input type="text" name="helpid" value="<?=isuserloggedin() ? $_SESSION['user_id'] : "";?>"></td>
		</tr>
		
		
	
</table>
<div style="text-align: center;">
			<input type="hidden" name="form_type" value="resolvedin">
			<input type="submit" name="submit" value="Submit Fix">
		</form>
</div>